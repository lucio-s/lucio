var Module = angular.module("assettApp", []);


Module.factory('getVideo', ['$http', function($http) {
    return {
        get: function() {
            return $http({
                method: 'GET',
                url: 'https://titan.asset.tv/api/channel-view-json/2240'
            });
        }
    };
}]);


Module.filter("startFrom",function(){

    return function(input, start) {
             if (!start)  return input;
            start+=start;

            return input.slice(start);
    };
});

Module.filter("searchModel",function(){

    return function(input, searchBox) {

        var result = [];
        if (!searchBox) 
            return input;

        
        searchBox = new RegExp(searchBox);

        for (var i=0, m=input.length; i<m; i++) {
                if (input[i].people.match(searchBox) || input[i].terms.match(searchBox) ) {
                    result.push(input[i]);
                }
        }

        return result;

    };

});


Module.controller('assettCtrl', ['$scope', '$http', 'getVideo', function($scope, $http, getVideo) {
    console.time("assettCtrl");


    $scope.videos = null;
    $scope.elementsOnPage = null;
    $scope.numberOfPages = null;
    $scope.search = null;
    $scope.filteredVideo =[];

    $scope.selected_video = null;
    $scope.showModal = $scope.selected_video !== null;
    

    getVideo.get().success(function(res) {
        console.log("yey " + res.content);
        //getting just the first set of video 
        var id = 0;
        for (var k in res.content) {
            if (id === 0) {
                $scope.videos = res.content[k];
            }
            id++;
            console.log(res.content[k].length, k);
        }


        whenResponseSuccess();
        console.timeEnd("assettCtrl");
    }).error(function(res) {
        console.log("$http call didn't work ", res);
        alert("Error loading data");
    });





    function whenResponseSuccess() {
        $scope.elementsOnPage = 10;
        $scope.numberOfPages = function() {
            return Math.ceil($scope.videos.length / $scope.elementsOnPage);
        };
        $scope.currentPage = 1;
        console.log("$scope.videos =" + $scope.videos.length);
    }
                            $scope.selectVideo = function (video) {
            console.log("Clickde", video );
            $scope.selected_video = video;
            $scope.showModal= true;
    };






}]);




Module.directive("assettVideo", function($compile, $timeout) {
    return {
        restrict: 'E',
        replace: true,

      
               compile: function(tEl, tAttr) {
                return {
                    pre: function(scope, iElem, iAttrs) {},
                    post: function(scope, iElem, iAttrs) {


                        $compile(iElem)(scope);}};},

        template: function(tElement, tAttrs) {
            return "<div class=\"col-lg-3 col-md-4 col-sm-6 col-xs-12\">" +
            "<span class=\"d-block mb-4 h-100\" >" +
            "<img class=\"img-fluid img-thumbnail\"   ng-src=\"{{video.image_url}}\" ng-click=\"selectVideo(video)\" >" +
            "</span>" +
            	"<div class=\"video-title\"> {{video.title}}</div>" +
            	"<div class=\"video-date\"> {{video.date}}</div>" +
            "</div>";
        }
    };

});