var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'AssetTV' , instruction : 'use /assettv.html', linkx : 'http://localhost:2117/assettv.html' });
});

module.exports = router;
